class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.text :description
      t.boolean :priority
      t.string :city
      t.integer :longitude
      t.integer :latitude
      t.string :state
      t.string :reporter_name
      t.integer :reporter_numero
      t.string :reporter_email

      t.timestamps
    end
  end
end
