class AddReportTypeIdToReport < ActiveRecord::Migration
  def change
    add_reference :reports, :report_type, index: true
  end
end
