class AddCompanyIdToReport < ActiveRecord::Migration
  def change
    add_reference :reports, :company, index: true
  end
end
