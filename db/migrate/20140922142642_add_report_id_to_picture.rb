class AddReportIdToPicture < ActiveRecord::Migration
  def change
    add_reference :pictures, :report, index: true
  end
end
