Rails.application.routes.draw do
  root to: 'reports#index'
  resources :reports
  resources :pictures
  resources :report_types
  resources :companies
  resources :users

  devise_for :users, :controllers => { registrations: 'registrations' }

end
