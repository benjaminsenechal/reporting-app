class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new user_params

    if @user.save
      redirect_to @user, as: :user, flash: { success: t('validation.create', model: @user.class.model_name.human.downcase) }
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to @user, as: :user, flash: { success: t('validation.update', model: @user.class.model_name.human.downcase) }
    else
      render 'edit'
    end
  end

  def destroy
    @user = Report.find(params[:id])
    if @user.present?
      @user.destroy
    end
    redirect_to users_url
  end

  private

  def user_params
    params.require(:user).permit(:email, :firstname, :lastname, :password, :password_confirmation, :company_id, :type)
  end
end
