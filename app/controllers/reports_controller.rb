class ReportsController < ApplicationController
  respond_to :html, :json
  skip_before_action :verify_authenticity_token

  def index
    if current_user.is_a? Admin
      @reports = Report.all
    else
      @reports = current_user.company.reports
    end
    respond_with @reports
  end

  def show
    @report = Report.find(params[:id])
    @pictures = @report.pictures.all
    respond_with @report
  end

  def new
    @report = Report.new
    @picture = @report.pictures.build
  end

  def edit
    @report = Report.find(params[:id])
  end

  def update
    @report = Report.find(params[:id])
    if @report.update report_params
      redirect_to reports_url, flash: { success: t('validation.update', model: @report) }
    else
      render 'edit'
    end
  end

  def create
    respond_to do |format|
      format.html do
        @report = Report.new report_params
        if @report.save
          params[:pictures]['path'].each do |a|
            @picture = @report.pictures.create!(:path => a, :report_id => @report.id)
          end
        end
      end
      format.json do
        @report = Report.new report_api_params
        if @report.save
          params[:pictures]['path'].each do |a|
            @picture = @report.pictures.create!(:path => a, :report_id => @report.id)
          end
        end
      end
    end

    if @report.save
      respond_to do |format|
        format.html { redirect_to reports_url, flash: { success: 'Report créé' } }
        format.json { render json: { callback: { content: @report.errors.full_messages, success: true } }}
      end
    else
      render json: { callback: { content: @report.errors.full_messages, success: false, status: 442 } }
    end
  end

  def destroy
    @report = Report.find(params[:id])
    if @report.present?
      @report.destroy
    end
    redirect_to reports_url
  end

  def report_params
    params.require(:report).permit(:name, :description, :priority, :city, :longitude, :latitude, :state, :reporter_name,
      :reporter_numero, :reporter_email, :report_type_id, pictures_attributes:[:id, :report_id, :path])
  end

  def report_api_params
    params.permit(:name, :description, :priority, :city, :longitude, :latitude, :state, :reporter_name,
      :reporter_numero, :reporter_email, :report_type_id, pictures_attributes: [:id, :report_id, :path])
  end

end
