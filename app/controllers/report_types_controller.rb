class ReportTypesController < ApplicationController
  before_action :set_report_type, only: [:show, :edit, :update, :destroy]

  def index
    @report_types = ReportType.all
  end

  def show
  end

  def new
    @report_type = ReportType.new
  end

  def edit
  end

  def create
    @report_type = ReportType.new(report_type_params)

    respond_to do |format|
      if @report_type.save
        format.html { redirect_to @report_type, notice: 'Report type was successfully created.' }
        format.json { render action: 'show', status: :created, location: @report_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @report_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @report_type.update(report_type_params)
        format.html { redirect_to @report_type, notice: 'Report type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @report_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @report_type.destroy
    respond_to do |format|
      format.html { redirect_to report_types_url }
      format.json { head :no_content }
    end
  end

  private
    def set_report_type
      @report_type = ReportType.find(params[:id])
    end

    def report_type_params
      params.require(:report_type).permit(:name, :private)
    end
end
