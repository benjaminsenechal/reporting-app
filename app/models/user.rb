class User < ActiveRecord::Base
  devise  :database_authenticatable,
          :recoverable,
          :registerable,
          :rememberable,
          :trackable,
          :validatable

  belongs_to :company

  validates :company_id,
            presence: true

  validates :type,
            presence: true

  validates :lastname,
            presence: true

  validates :firstname,
            presence: true
end
