class Report < ActiveRecord::Base
  extend Enumerize

  belongs_to :company
  has_many :pictures
  belongs_to :report_type

  enumerize :state, in: [:received, :in_progress, :archived], default: :received

  validates :name,
            presence: true

  default_scope { order('reports.created_at DESC') }

end
