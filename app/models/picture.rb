class Picture < ActiveRecord::Base
  mount_uploader :path, AttachmentUploader
  belongs_to :report
end
